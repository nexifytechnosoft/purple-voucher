<!DOCTYPE html>
<html lang="en">
<head>
<?php echo $headerTag; ?>

</head>
<body>
<!-- This part will be container of all the elements except may be the nav part -->
<div class="container">
      <h2>Star-empty Glyph</h2>
      <p>Star-empty icon: <span class="glyphicon glyphicon-star-empty"></span></p>    
      <p>Star-empty icon as a link:
        <a href="#">
          <span class="glyphicon glyphicon-star-empty"></span>
        </a>
      </p>
      <p>Star-empty icon on a button:
        <button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-star-empty"></span> Star
        </button>
      </p>
      <p>Star-empty icon on a styled link button:
        <a href="#" class="btn btn-info btn-lg">
          <span class="glyphicon glyphicon-star-empty"></span> Star
        </a>
      </p> 
</div>
</body>
</html>