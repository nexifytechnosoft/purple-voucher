<meta charset="UTF-8">
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<?php for ($i=0; $i < count($jsfiles); $i++) { ?>
	<script type="text/javascript" src="<?php echo $jsfiles[$i]; ?>"></script>
<?php } ?>
<?php for ($i=0; $i < count($cssfiles); $i++) { ?>
	<link rel="stylesheet" href="<?php echo $cssfiles[$i]; ?>">
<?php } ?>