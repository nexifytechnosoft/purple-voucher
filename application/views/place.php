<!DOCTYPE html>
<html lang="en">
   <head>
        <?php echo $headerTag; ?>

        <script type="text/javascript">
            $.getJSON("allRestaurants", function(result){
                for (var i = 0; i < result.length; i++) {
                console.log(result[i]);
                };
            });
            // Starrr plugin (https://github.com/dobtco/starrr)
            var __slice = [].slice;

            (function($, window) {
              var Starrr;

              Starrr = (function() {
                Starrr.prototype.defaults = {
                  rating: void 0,
                  numStars: 5,
                  change: function(e, value) {}
                };

                function Starrr($el, options) {
                  var i, _, _ref,
                    _this = this;

                  this.options = $.extend({}, this.defaults, options);
                  this.$el = $el;
                  _ref = this.defaults;
                  for (i in _ref) {
                    _ = _ref[i];
                    if (this.$el.data(i) != null) {
                      this.options[i] = this.$el.data(i);
                    }
                  }
                  this.createStars();
                  this.syncRating();
                  this.$el.on('mouseover.starrr', 'span', function(e) {
                    return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
                  });
                  this.$el.on('mouseout.starrr', function() {
                    return _this.syncRating();
                  });
                  this.$el.on('click.starrr', 'span', function(e) {
                    return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
                  });
                  this.$el.on('starrr:change', this.options.change);
                }

                Starrr.prototype.createStars = function() {
                  var _i, _ref, _results;

                  _results = [];
                  for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                    _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
                  }
                  return _results;
                };

                Starrr.prototype.setRating = function(rating) {
                  if (this.options.rating === rating) {
                    rating = void 0;
                  }
                  this.options.rating = rating;
                  this.syncRating();
                  return this.$el.trigger('starrr:change', rating);
                };

                Starrr.prototype.syncRating = function(rating) {
                  var i, _i, _j, _ref;

                  rating || (rating = this.options.rating);
                  if (rating) {
                    for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                      this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
                    }
                  }
                  if (rating && rating < 5) {
                    for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                      this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                    }
                  }
                  if (!rating) {
                    return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                  }
                };

                return Starrr;

              })();
              return $.fn.extend({
                starrr: function() {
                  var args, option;

                  option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                  return this.each(function() {
                    var data;

                    data = $(this).data('star-rating');
                    if (!data) {
                      $(this).data('star-rating', (data = new Starrr($(this), option)));
                    }
                    if (typeof option === 'string') {
                      return data[option].apply(data, args);
                    }
                  });
                }
              });
            })(window.jQuery, window);

            $(function() {
              return $(".starrr").starrr();
            });

            $( document ).ready(function() {
                  
              $('#hearts').on('starrr:change', function(e, value){
                $('#count').html(value);
              });
              
              $('#hearts-existing').on('starrr:change', function(e, value){
                $('#count-existing').html(value);
              });
            });
        </script>
        <style type="text/css">
        .filter .thumbnail{
            border: none;background: rgba(147,206,222,1);
            background: -moz-linear-gradient(top, rgba(147,206,222,1) 0%, rgba(147,206,222,1) 24%, rgba(117,189,209,1) 25%, rgba(73,165,191,1) 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(147,206,222,1)), color-stop(24%, rgba(147,206,222,1)), color-stop(25%, rgba(117,189,209,1)), color-stop(100%, rgba(73,165,191,1)));
            background: -webkit-linear-gradient(top, rgba(147,206,222,1) 0%, rgba(147,206,222,1) 24%, rgba(117,189,209,1) 25%, rgba(73,165,191,1) 100%);
            background: -o-linear-gradient(top, rgba(147,206,222,1) 0%, rgba(147,206,222,1) 24%, rgba(117,189,209,1) 25%, rgba(73,165,191,1) 100%);
            background: -ms-linear-gradient(top, rgba(147,206,222,1) 0%, rgba(147,206,222,1) 24%, rgba(117,189,209,1) 25%, rgba(73,165,191,1) 100%);
            background: linear-gradient(to bottom, rgba(147,206,222,1) 0%, rgba(147,206,222,1) 24%, rgba(117,189,209,1) 25%, rgba(73,165,191,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#93cede', endColorstr='#49a5bf', GradientType=0 );
        }

        #hearts { color: #ee8b2d;}
        #hearts-existing { color: #ee8b2d;}
        </style>
   </head>
   <body>
      <!-- This part will be container of all the elements except may be the nav part -->
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-2 filter" style="background: rgba(1, 1, 1, 0.8);height: 1000px;border-top: 24px solid rgb(211, 27, 69);">
                <div class="col-md-12" style="padding:10px">
                    <div class="thumbnail">
                        <div class="caption">
                            <h4 style="">
                                Dishes
                            </h4>
                            <div class="checkbox">
                                <label>
                                    <input class="dishes" type="checkbox" value="north"> North Indian
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="dishes" type="checkbox" value="north"> South Indian
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="dishes" type="checkbox" value="north"> Chinese Food
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="dishes" type="checkbox" value="north"> Thai Food
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="dishes" type="checkbox" value="north"> Non Veg
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding:10px">
                    <div class="thumbnail">
                        <div class="caption">
                            <h4 style="">
                                Rating
                            </h4>
                            <div class="checkbox">
                                <label>
                                    <input class="rating" type="checkbox" value="north"> 5 Star
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="rating" type="checkbox" value="north"> 4 Star
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="rating" type="checkbox" value="north"> 3 Star
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="rating" type="checkbox" value="north"> 2 Star
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="rating" type="checkbox" value="north"> 1 Star
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding:10px">
                    <div class="thumbnail">
                        <div class="caption">
                            <h4 style="">
                                City
                            </h4>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Bangalore
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Noida
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Delhi
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Mumbai
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Agra
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding:10px">
                    <div class="thumbnail">
                        <div class="caption">
                            <h4 style="">
                                City
                            </h4>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Deluxe
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Supreme
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Normal
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Super Deluxe
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="city" type="checkbox" value="north"> Hi-Fi
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding:10px">
                    <div class="thumbnail">
                        <div class="caption">
                            <h4 style="">
                                Others
                            </h4>
                            <div class="checkbox">
                                <label>
                                    <input class="type" type="checkbox" value="wifi"> Wi-Fi
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="type" type="checkbox" value="ac"> Air conditioned
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="type" type="checkbox" value="music"> Music
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="type" type="checkbox" value="dance"> Dance Shows
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
               <!-- <div class="checkbox">
               		<table>
                  <tr><td><input type="checkbox" name="north" id="north" onclick="myFunction(this)" value="north">North Indian</td></tr>
                  <tr><td><input type="checkbox" name="south" id="south" value="south" onclick="myFunction(this)">South Indian</td></tr>
                  <tr><td><input type="checkbox" name="chinese" id="chinese" value="chinese" onclick="myFunction(this)">Chinese Food</td></tr>
                  <tr><td><input type="checkbox" name="thai" id="thai" value="thai" onclick="myFunction(this)">Thai Food</td></tr>
                  <tr><td><input type="checkbox" name="nonveg" id="nonveg" value="nonveg" onclick="myFunction(this)">Non Veg</td></tr>
                   </table>
                </div>
                <div id="div2" class="rating">
                    <h2 id="rating">Rating</h2>
                    <table>
                  <tr><td><input type="checkbox" name="rating" value="5" id="5" onclick="my_rating(this)">5 Star</td></tr>
                  <tr><td><input type="checkbox" name="rating" value="4" id="4" onclick="my_rating(this)">4 Star</td></tr>
                  <tr><td><input type="checkbox" name="rating" value="3" id="3" onclick="my_rating(this)">3 Star </td></tr>
                  <tr><td><input type="checkbox" name="rating" value="2" id="2" onclick="my_rating(this)">2 Star</td></tr>
                  <tr><td><input type="checkbox" name="rating" value="1" id="1" onclick="my_rating(this)">1 Star</td></tr>
           </table> 
                </div>
                 <div id="div3" class="city">
                    <h2 id="city">City</h2>
                    <table>
                  <tr><td><input type="checkbox" name="city" value="bangalore">Bangalore</td></tr>
                  <tr><td><input type="checkbox" name="city" value="noida">Noida</td></tr>
                  <tr><td><input type="checkbox" name="city" value="delhi">Delhi</td></tr>
                  <tr><td><input type="checkbox" name="city" value="mumbai">Mumbai</td></tr>
                  <tr><td><input type="checkbox" name="city" value="agra">Agra</td></tr>
           </table> 
                </div>
                 <div id="div4" class="type">
                    <h2 id="type">Type</h2>
                    <table>
                  <tr><td><input type="checkbox" name="type" value="deluxe">Deluxe</td></tr>
                  <tr><td><input type="checkbox" name="type" value="supreme">Supreme</td></tr>
                  <tr><td><input type="checkbox" name="type" value="">Normal</td></tr>
                  <tr><td><input type="checkbox" name="type" value="2">Super Deluxe</td></tr>
                  <tr><td><input type="checkbox" name="type" value="1">Hi-Fi</td></tr>
           </table> 
                </div>
               <div id="div5" class="others">
                    <h2 id="type">Others</h2>
                    <table>
                  <tr><td><input type="checkbox" name="others" value="wifi">Wi-Fi</td></tr>
                  <tr><td><input type="checkbox" name="others" value="ac">Air conditioned</td></tr>
                  <tr><td><input type="checkbox" name="others" value="music">Music</td></tr>
                  <tr><td><input type="checkbox" name="others" value="dance">Dance Shows</td></tr>
           </table> 
				</div> -->
            </div>
            <div class="col-md-10">
				<div class="row">
                  <div class = "col-sm-6 col-md-3">
                     <div class = "thumbnail">
                        <img width="250px" height="250px" src="images\Koala.jpg" alt = "Hotel Picture">
                     </div>
                     <div class = "caption">
                        <h3>Hotel Name</h3>
                        <p>Some sample text. Some sample text.</p>
                        <p>
                           <a href = "#" class = "btn btn-primary" role = "button">
                           Button
                           </a> 
                           <a href = "#" class = "btn btn-default" role = "button">
                           Button
                           </a>
                        </p>
                        <div class="container">
                           <div class="row">
                           </div>
                           <div class="row lead">
                              <div id="hearts-existing" class="starrr" data-rating='4'></div><span id="count-existing" style='font-size:16px'>4</span> star(s)
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>