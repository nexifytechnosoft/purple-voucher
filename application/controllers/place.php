<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class place extends CI_Controller {
	public function index()
	{
		$data['headerTag'] = $this->setCommonHeadTag();
		$this->load->view('place',$data);
	}

	public function setCommonHeadTag()
	{
		$commonheaderdata['title'] = "Purple Voucher";
		$commonheaderdata['jsfiles'] = array(
										);
		$commonheaderdata['cssfiles'] = array(
											"home/home.css"
										);
		return $this->load->view('headerfiles/commonHeaderTag',$commonheaderdata,true);
	}
}
