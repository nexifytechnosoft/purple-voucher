<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
	public function index()
	{
		$data['headerTag'] = $this->setCommonHeadTag();
		$this->load->view('home',$data);
	}

	public function setCommonHeadTag()
	{
		$commonheaderdata['title'] = "Purple Voucher";
		$commonheaderdata['jsfiles'] = array(
										);
		$commonheaderdata['cssfiles'] = array(
										);
		return $this->load->view('headerfiles/commonHeaderTag',$commonheaderdata,true);
	}

	public function login()
	{
		$userData['name'] = $this->input->post('name');
		$userData['password'] = $this->input->post('password');
		$this->load->model('model_home');
		$result = $this->model_home->abcd($userData);
		print_r($result);
		redirect('home');
	}
}
